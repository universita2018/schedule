<?php
    session_start();
    if(isset($_SESSION['user'])){
        if($_SESSION['user']['typeAccount']!="Admin"){
            header('Location: ../usuario/');
        }
    }else {
            header('Location:../index.php');
        }
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="../css/estilos.css">

    <link rel="stylesheet" href="../css/font-awesome.css">
    <script src="../js/jquery-3.2.1.js"></script>
	<script src="../js/main.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    

    <title>INICIO</title>
</head>
<body>
    <header>
        <span id="button-menu" class="fa fa-bars"></span>
        <nav class="navegacion">
            
            <ul class="menu">
                <li class="title-menu">Todas las categorias</li>
				<!-- TITULAR -->

                        <li><a href="RCarreer.php">Carrera</a></li>
                        <li><a href="Rcourses.php"><span class="fas fa-graduation-cap"></span>Curso</a><li>
                        <li><a href="RGroups.php"><span class="far fa-folder-open"></span>Grupos</a></li>
                        <li><a href="RPeriod.php"><span class="far fa-calendar-check"></span>Periodo</a></li>
                        <li><a href="RClassroom.php"><span class="fas fa-school"></span>Aulas</a></li>
                        <li><a href="RStudent.php"><span class="fas fa-user-graduate"></span>Estudiantes</a></li>
                        <li><a href="RTeacher.php"><span  class="fas fa-chalkboard-teacher"></span>Profesores</a></li>
                        <li><a href="#"><span class="fas fa-user"></span>Usuarios</a></li>
                  
            </ul>
        </nav>
    </header>
    <div id="container">
    </div>

</body>
</html>