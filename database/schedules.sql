-- MySQL Script generated by MySQL Workbench
-- 11/27/18 22:56:15
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema schedule
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `schedule` ;

-- -----------------------------------------------------
-- Schema schedule
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `schedule` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `schedule` ;

-- -----------------------------------------------------
-- Table `schedule`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`student` (
  `idstudent` INT NOT NULL,
  `firstname_student` VARCHAR(45) NULL,
  `lastname_student` VARCHAR(45) NULL,
  `birth_student` VARCHAR(45) NULL,
  `ci_student` INT(7) NULL,
  `phone_student` INT(10) NULL,
  `studentcol` VARCHAR(45) NULL,
  PRIMARY KEY (`idstudent`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`teacher` (
  `idteacher` INT NOT NULL,
  `name_teacher` VARCHAR(45) NULL,
  `subjects_teacher` VARCHAR(45) NULL,
  `phone_teacher` VARCHAR(45) NULL,
  `labor_hours_teacher` VARCHAR(45) NULL,
  PRIMARY KEY (`idteacher`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`account` (
  `idaccount` INT NOT NULL AUTO_INCREMENT,
  `emailAccount` VARCHAR(45) NULL,
  `passAccount` VARCHAR(20) NULL,
  `usernameAccount` VARCHAR(45) NULL,
  `typeAccount` VARCHAR(45) NULL,
  `statusAccount` VARCHAR(45) NULL,
  PRIMARY KEY (`idaccount`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`course` (
  `idcourse` INT NOT NULL AUTO_INCREMENT,
  `name_course` VARCHAR(45) NULL,
  `avaliable_course` VARCHAR(45) NULL,
  PRIMARY KEY (`idcourse`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`classroom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`classroom` (
  `idclassroom` INT NOT NULL AUTO_INCREMENT,
  `name_classroom` VARCHAR(45) NULL,
  `type_classroom` VARCHAR(45) NULL,
  `address_classroom` VARCHAR(45) NULL,
  PRIMARY KEY (`idclassroom`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`period`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`period` (
  `idperiod` INT NOT NULL AUTO_INCREMENT,
  `hour` VARCHAR(45) NULL,
  `hour_exit` VARCHAR(45) NULL,
  PRIMARY KEY (`idperiod`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`course_classroom`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`course_classroom` (
  `idcourse_classroom` INT NOT NULL AUTO_INCREMENT,
  `classroom_idclassroom` INT NOT NULL,
  `course_idcourse` INT NOT NULL,
  PRIMARY KEY (`idcourse_classroom`),
  INDEX `fk_course_classroom_classroom1_idx` (`classroom_idclassroom` ASC),
  INDEX `fk_course_classroom_course1_idx` (`course_idcourse` ASC),
  CONSTRAINT `fk_course_classroom_classroom1`
    FOREIGN KEY (`classroom_idclassroom`)
    REFERENCES `schedule`.`classroom` (`idclassroom`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_classroom_course1`
    FOREIGN KEY (`course_idcourse`)
    REFERENCES `schedule`.`course` (`idcourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`admin` (
  `id_admin` INT NOT NULL,
  `login_adm` VARCHAR(45) NULL,
  `pass_adm` VARCHAR(45) NULL,
  PRIMARY KEY (`id_admin`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`course_teacher`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`course_teacher` (
  `idcourse_teacher` INT NOT NULL,
  `course_idcourse` INT NOT NULL,
  `teacher_idteacher` INT NOT NULL,
  PRIMARY KEY (`idcourse_teacher`),
  INDEX `fk_course_teacher_course1_idx` (`course_idcourse` ASC),
  INDEX `fk_course_teacher_teacher1_idx` (`teacher_idteacher` ASC),
  CONSTRAINT `fk_course_teacher_course1`
    FOREIGN KEY (`course_idcourse`)
    REFERENCES `schedule`.`course` (`idcourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_teacher_teacher1`
    FOREIGN KEY (`teacher_idteacher`)
    REFERENCES `schedule`.`teacher` (`idteacher`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`course_student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`course_student` (
  `idcourse_student` INT NOT NULL AUTO_INCREMENT,
  `course_idcourse` INT NOT NULL,
  `student_idstudent` INT NOT NULL,
  PRIMARY KEY (`idcourse_student`),
  INDEX `fk_course_student_course1_idx` (`course_idcourse` ASC),
  INDEX `fk_course_student_student1_idx` (`student_idstudent` ASC),
  CONSTRAINT `fk_course_student_course1`
    FOREIGN KEY (`course_idcourse`)
    REFERENCES `schedule`.`course` (`idcourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_student_student1`
    FOREIGN KEY (`student_idstudent`)
    REFERENCES `schedule`.`student` (`idstudent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`carreer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`carreer` (
  `idcarreer` INT NOT NULL,
  `sub_carreer` VARCHAR(45) NULL,
  `years_carreer` INT(2) NULL,
  `num_course_carrer` INT(3) NULL,
  PRIMARY KEY (`idcarreer`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`group` (
  `idgroup` INT NOT NULL,
  `carreer_idcarreer` INT NOT NULL,
  `course_idcourse` INT NOT NULL,
  PRIMARY KEY (`idgroup`),
  INDEX `fk_group_carreer1_idx` (`carreer_idcarreer` ASC),
  INDEX `fk_group_course1_idx` (`course_idcourse` ASC),
  CONSTRAINT `fk_group_carreer1`
    FOREIGN KEY (`carreer_idcarreer`)
    REFERENCES `schedule`.`carreer` (`idcarreer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_course1`
    FOREIGN KEY (`course_idcourse`)
    REFERENCES `schedule`.`course` (`idcourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`status` (
  `idstatus` INT NOT NULL,
  `idcourse` VARCHAR(45) NULL,
  `nameCourse_status` VARCHAR(45) NULL,
  `totalscore_status` VARCHAR(45) NULL,
  `approbal_status` VARCHAR(45) NULL,
  `course_idcourse` INT NOT NULL,
  PRIMARY KEY (`idstatus`, `course_idcourse`),
  INDEX `fk_status_course_idx` (`course_idcourse` ASC),
  CONSTRAINT `fk_status_course`
    FOREIGN KEY (`course_idcourse`)
    REFERENCES `schedule`.`course` (`idcourse`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `schedule`.`score`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `schedule`.`score` (
  `idscore` INT NOT NULL,
  PRIMARY KEY (`idscore`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
