jQuery(document).on('submit','#form',function(){
    event.preventDefault();
    jQuery.ajax({
        url:'verificar.php',
        type:'POST',
        dataType:'json',
        data: $(this).serialize(),
        beforeSend:function(){

        }
    })
    .done(function(respuesta){
        console.log(respuesta);
        if(!respuesta.error){
            if(respuesta.tipo='Admin'){
                location.href='admin/index.php';
            }
            else if(respuesta.tipo='Usuario'){
                location.href='usuario/';
            }
        }else{
            $('.error').slideDonw('slow');
            setTimeout(function(){
                $('.error').slideUp('slow');
                $('.boton').val('iniciar sesion');
            },3000);
         }
    })
    .fail(function(resp){
        console.log(resp.responseText);
    })
    .always(function(){
        console.log("completo");
    });
});