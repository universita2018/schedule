<?php
    session_start();
    if(isset($_SESSION['user'])){
        if($_SESSION['user']['typeAccount']=="Admin"){
            header('Location: admin/');
        }else if($_SESSION['user']['typeAccount']=="Usuario"){
            header('Location:usuario/');
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css">
    <title>Cuentas de Usuarios</title>

</head>
<body>
    <div class="error"><span>Datos no validos,intente de nuevo</span></div>
    <div class="main">
        <form action="" id=form>
            <label for="usuario">Login</label>
            <input type="text" name="usuario" id="usuario" pattern="[A-Za-z0-9_-]{1,15}"required>
            <label for="pass">Contraseña</label>
            <input type="password" name="pass" id="pass" pattern="[A-Za-z0-9]{1,15}" required> 
            <input type="submit" class="boton" value="acceder">    
        </form>
    </div>
    <script src="js/jquery-3.2.1.js"></script>
    <script src="js/login.js"></script>
</body>
</html>